//
//  DashboardViewController.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/24/21.
//

import UIKit

class DashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let reportCellText = ["Roadside Assitance Request" , "Report Accident"]
    
    var userDataArray: [UserModel] = [UserModel]()
    var userDetailArray: Array = [String]()
    var statesArray: [StateModel] = [StateModel]()


    lazy var tableView:UITableView = {
       let v = UITableView()
        v.separatorStyle = .singleLine
        return v
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "DASHBOARD"
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "Profile.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(profileClicked), for: .touchUpInside)

        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems = [barButton]

        setupUI()
    }


    @objc func profileClicked()  {
        let profileViewController = ProfileViewController()
        profileViewController.userArray = userDataArray
        profileViewController.userDetailArray = userDetailArray
        profileViewController.stateArray = statesArray
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    func setupUI() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(BillTableViewCell.self, forCellReuseIdentifier: BillTableViewCell.cellId)
        tableView.register(CarouselTableViewCell.self, forCellReuseIdentifier: CarouselTableViewCell.cellId)
        tableView.register(RRTableViewCell.self, forCellReuseIdentifier: RRTableViewCell.cellId)
        tableView.register(AgentTableViewCell.self, forCellReuseIdentifier: AgentTableViewCell.cellId)
        self.view.addSubview(tableView)
       
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 2 {
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 2 {
            return 50
        }
        
        return 150 // hardcoded // will update
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Upcoming Bills"
        } else if section == 1 {
            return "My IDs"
        } else if section == 2 {
            return "Report /Request"
        } else if section == 3 {
            return "Contact Agent"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: BillTableViewCell.cellId, for: indexPath) as! BillTableViewCell
            //cell.billImgView.downloaded(from: "https://randomuser.me/api/portraits/thumb/women/75.jpg")
            cell.billImgView.image = UIImage.init(named: "ic_onboarding_1")
          
            return cell
        } else if indexPath.section  == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CarouselTableViewCell.cellId, for: indexPath) as! CarouselTableViewCell
//           cell.slider.imageView.downloaded(from: "https://randomuser.me/api/portraits/thumb/women/75.jpg")
            return cell
        } else if indexPath.section  == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: RRTableViewCell.cellId, for: indexPath) as! RRTableViewCell
            cell.requestTitle.text = reportCellText[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            return cell
        } else if indexPath.section  == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgentTableViewCell.cellId, for: indexPath) as! AgentTableViewCell
            
            if userDataArray.count > 0 {
                cell.agentNameLbl.text = "\(userDataArray[indexPath.row].username.title) \(userDataArray[indexPath.row].username.name) "
                cell.agentNumberLbl.text = userDataArray[indexPath.row].phone
                cell.agentImgView.downloaded(from: userDataArray[indexPath.row].picture.large)
            }
 
            return cell
        }
       return UITableViewCell()
    }
}
