//
//  AgentTableViewCell.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/25/21.
//

import UIKit

class AgentTableViewCell: UITableViewCell {

    static var cellId = "AgentTableViewCell"
    
    let agentNameLbl: UILabel = {
       let v = UILabel()
        v.textColor = .black
     
        v.textAlignment = .left
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        //v.text = "Tulasi"
        return v
        
    }()
    
    let agentNumberLbl: UILabel = {
       let v = UILabel()
       v.textColor = .black
      
        v.textAlignment = .left
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
       // v.text = "989219921"
        return v
        
    }()
    
    let agentImgView: UIImageView = {
       let imgV = UIImageView()
        imgV.layer.borderWidth = 3.0
        imgV.layer.cornerRadius = 10.0
        imgV.layer.borderColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        return imgV
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupUI() {
        self.addSubview(agentImgView)
        addSubview(agentNameLbl)
        addSubview(agentNumberLbl)
       
        agentImgView.snp.makeConstraints { (make) in
            make.top.equalTo(10.0)
            make.leftMargin.equalTo(5)
            make.height.width.equalTo(50)
        }
        agentNameLbl.snp.makeConstraints { (make) in
            make.topMargin.equalTo(15)
            make.leftMargin.equalTo(agentImgView.snp.right).offset(20.0)
            make.rightMargin.equalTo(-10)
        }
        agentNumberLbl.snp.makeConstraints { (make) in
            make.topMargin.equalTo(agentNameLbl.snp.bottom).offset(10.0)
            make.leftMargin.equalTo(agentImgView.snp.right).offset(20.0)
            make.rightMargin.equalTo(-10)
        }
    }

}
