//
//  ViewController.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/26/21..
//

import UIKit
import SnapKit
import PromiseKit
import Alamofire
import SwiftyJSON


class LoginViewController: UIViewController,UITextFieldDelegate {
    
    
    var userArray: [UserModel] = [UserModel]()
    var userDetailArray: Array = [String]()
    var stateArray: [StateModel] = [StateModel]()

    lazy var loginViewBGview: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 15
        view.backgroundColor = .lightGray
        view.layer.opacity = 0.7
        return view
    }()
    
    lazy var userNameLbl: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        label.textAlignment = .left
        label.text = "User ID"
        label.textColor = .black
        return label
    }()
    lazy var pwdLbl: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        label.textAlignment = .left
        label.text = "Password"
        label.textColor = .black
        return label
    }()
    
    lazy var userNameTextField : UITextField = {
        let utextField = UITextField()
        utextField.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        utextField.placeholder = "User ID"
        //utextField.text = "eve.holt@reqres.in"
        utextField.borderStyle = UITextField.BorderStyle.roundedRect
        utextField.textColor = .black
        return utextField
    }()
    
    lazy var passwordTextField : UITextField = {
        let ptextField = UITextField()
        ptextField.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        ptextField.placeholder = "Password"
        ptextField.borderStyle = UITextField.BorderStyle.roundedRect
        //ptextField.text = "1"
        ptextField.isSecureTextEntry = true
        ptextField.textColor = .black
        return ptextField
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.setTitle("SIGN IN!", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(signInpressed), for: .touchUpInside)
        return button
    }()
    lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        label.textAlignment = .center
        label.text = "Login Error!"
        label.textColor = .red
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.addSubview(loginViewBGview)
        loginViewBGview.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.leftMargin.equalTo(10)
            make.rightMargin.equalTo(-10)
            make.height.equalTo(140)
        }
        self.loginViewBGview.addSubview(userNameLbl)
        userNameLbl.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(1.0 / 4.0)
            make.leftMargin.equalTo(10)
            make.topMargin.equalTo(20)
            make.height.equalTo(44)
        }
        self.loginViewBGview.addSubview(pwdLbl)
        pwdLbl.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(1.0 / 4.0)
            make.top.equalTo(userNameLbl.snp.bottom).offset(10.0)
            make.leftMargin.equalTo(10)
            make.height.equalTo(44)
        }
        self.loginViewBGview.addSubview(userNameTextField)
        userNameTextField.snp.makeConstraints { (make) in
            make.topMargin.equalTo(20)
            make.leftMargin.equalTo(userNameLbl.snp.right).offset(10.0)
            make.rightMargin.equalTo(-10)
        }
        
        self.loginViewBGview.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(userNameTextField.snp.bottom).offset(20.0)
            make.leftMargin.equalTo(pwdLbl.snp.right).offset(10.0)
            make.rightMargin.equalTo(-10)
        }
        self.view.addSubview(errorLabel)
        errorLabel.snp.makeConstraints { (make) in
            make.top.equalTo(loginViewBGview.snp.bottom).offset(20.0)
            make.leftMargin.equalTo(20)
            make.rightMargin.equalTo(-20)
            errorLabel.isHidden = true
        }
        self.view.addSubview(loginButton)
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(errorLabel.snp.bottom).offset(10.0)
            make.leftMargin.equalTo(10)
            make.rightMargin.equalTo(-10)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.errorLabel.isHidden = true
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.errorLabel.isHidden = true
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //self.errorLabel.isHidden = true
    }
    
    
    @objc func signInpressed(_ sender: UIButton) {
        let apiClient = ApiClient()
        let userName = userNameTextField.text ?? ""
        let passwrd = passwordTextField.text ?? ""
        let loginrequest: [String: Any] = apiClient.requestForLogin(userName: userName, password: passwrd)
        var isValid = false
        
        if userName.count > 0 && passwrd.count > 0 {
            Services.sharedInstance.alamoFirePostRequestWithUrl(jsonBody: loginrequest as [String: AnyObject], subUrl: "/login") { (response, error, statusCode) in
                var jsonString = ""
                self.errorLabel.isHidden = true
                if statusCode == 200{
                    print(response as Any)
                    do {
                        let jsonData: Data = try JSONSerialization.data(withJSONObject: response as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                        jsonString = String(data: jsonData, encoding: String.Encoding.utf8) ?? "No Response"
                        isValid  = true
                    }catch {
                        
                    }
                    if (response) != nil{
                        isValid = true
                        print(jsonString)
                        
                        self.sendUserInfoRequest()

                    }
                }
                if !isValid {
                    self.errorLabel.isHidden = false
                }
            }
        } else {
            self.errorLabel.isHidden = false
        }
        
    }
    
    func sendUserInfoRequest() {
        // Alamofire, PromiseKit, SwiftyJSON
        self.getUserInfo()
            .done { json -> Void in
                // Do something with the JSON info
                let value = JSON(json)
                
           
                
                for (_, data) in value["results"] {
                    
                    let userId = data["id"]["name"].string ?? ""
                    
                    if userId.count > 0 {
                        let title = data["name"]["title"].string ?? ""
                        let first = data["name"]["first"].string ?? ""
                        let last = data["name"]["last"].string ?? ""
                      
                        
                        let city = data["location"]["city"].string ?? ""
                        let state = data["location"]["state"].string ?? ""
                        let country = data["location"]["country"].string ?? ""
                        let postcode = data["location"]["postcode"].int ?? 0
                        
                        let streetNumber = data["location"]["street"]["number"].int ?? 0
                        let streetName = data["location"]["street"]["name"].string ?? ""
                        
                       
                        
                        let email = data["email"].string ?? ""
                        let phone = data["phone"].string ?? ""
                        
                        
                        let id = data["id"]["name"].string ?? ""
                        let value = data["id"]["value"].string ?? ""
                       
                        let largeImage = data["picture"]["large"].string ?? ""
                        let mediumImage = data["picture"]["medium"].string ?? ""
                        let thumnbailImage = data["picture"]["thumbnail"].string ?? ""
                       
                        
                        
                        self.userArray.append(UserModel(username: UserName(title: title, name: "\(first) \(last)"), location: Location(street: Street(number: String(streetNumber), name: streetName), city: city, state: state, country: country, postcode: String(postcode)), email: email, phone: phone, picture: Picture(large: largeImage, medium: mediumImage, thumbnail: thumnbailImage), id: Id(name: id, value: value)))
                        
                        self.userDetailArray.append(streetName)
                        self.userDetailArray.append(String(streetNumber))
                        self.userDetailArray.append(city)
                        self.userDetailArray.append(state)
                        self.userDetailArray.append(country)
                        self.userDetailArray.append(String(postcode))
                        self.userDetailArray.append(phone)
                        self.userDetailArray.append(email)
                        
                        self.loadTabBar()

                    } else {
                        
                        let alert = UIAlertController(title: "ID not found", message: "Unable to retrieve data at this time, please try again after sometime", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                            print("User click Ok button")
                            
                        }))
                        
                        self.present(alert, animated: true, completion: {
                            print("completion block")
                            

                        })
                    }
                }
                
                debugPrint(value.dictionaryValue.values)
                
            }
            .catch { error in
                // Handle error or give feedback to the user
                debugPrint(error.localizedDescription)
            }
       
        self.getStateInfo()
            .done { json -> Void in
                // Do something with the JSON info
                let value = JSON(json)
              
                for (_, dataValues) in value["data"] {
                    
                    let state = dataValues["State"].string ?? ""
                    let id = dataValues["ID State"].string ?? ""
                    self.stateArray.append(StateModel(id: id, state: state))
                }
            }
            .catch { error in
                
            }

    }
        
    func getUserInfo() -> Promise<[String: Any]> {
        return Promise { seal in
            
            let url = "https://randomuser.me/api/"
            var parameters: Parameters = [:]
            
            parameters = [
                "exc": "login,dob,registered,gender,nat,cell,timezone"
            ]
            AF.request(
                url,
                method: .get,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: nil).responseJSON { response in
                    
                    switch response.result {
                    case .success(let json):
                        guard let json = json  as? [String: Any] else {
                            return seal.reject(AFError.responseValidationFailed(reason: .dataFileNil))
                        }
                        seal.fulfill(json)
                    case .failure(let error):
                        seal.reject(error)
                    }
                }
        }
    }
    
    func getStateInfo() -> Promise<[String: Any]> {
        return Promise { seal in
            
            let url = "https://datausa.io/api/data"
            var parameters: Parameters = [:]
            parameters = [
                "drilldowns": "State",
                "measures": "Population",
                "limit":"5"
            ]
            AF.request(
                url,
                method: .get,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: nil).responseJSON { response in
                    
                    switch response.result {
                    case .success(let json):
                        guard let json = json  as? [String: Any] else {
                            return seal.reject(AFError.responseValidationFailed(reason: .dataFileNil))
                        }
                        seal.fulfill(json)
                    case .failure(let error):
                        seal.reject(error)
                    }
                }
        }
    }
    
    func loadTabBar() {
        let dashBoardVieController = DashboardViewController()
        dashBoardVieController.userDataArray = userArray
        dashBoardVieController.userDetailArray = userDetailArray
        dashBoardVieController.statesArray = stateArray
        
        let myPoliciesViewController = MyPoliciesViewController()
        let thirdController = SampleController()
        let fourthController = SampleController()
        let fifthController = SampleController()
        
        let dashBoardNav = UINavigationController.init(rootViewController: dashBoardVieController)
        let mypoliciesNav = UINavigationController.init(rootViewController: myPoliciesViewController)
        let thirdNav = UINavigationController.init(rootViewController: thirdController)
        let fourthNav = UINavigationController.init(rootViewController: fourthController)
        let fifthNav = UINavigationController.init(rootViewController: fifthController)
        
        let tabBarController = UITabBarController()
        tabBarController.tabBar.barTintColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        
        let dashBoardTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "album-simple-7")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "album-simple-7"))
        dashBoardNav.tabBarItem = dashBoardTabBarItem
        
        let mypoliciesTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "server-7")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "server-7"))
        mypoliciesNav.tabBarItem = mypoliciesTabBarItem
        
        let thirdTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "square-individual-nine-7")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "square-individual-nine-7"))
        thirdNav.tabBarItem = thirdTabBarItem
        
        let fourthTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "update-7")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "update-7"))
        fourthNav.tabBarItem = fourthTabBarItem
        
        let fifthTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "text-list-7")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "text-list-7"))
        fifthNav.tabBarItem = fifthTabBarItem
        
        tabBarController.viewControllers = [dashBoardNav, mypoliciesNav, thirdNav,fourthNav,fifthNav]
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(tabBarController)
        
    }

}

