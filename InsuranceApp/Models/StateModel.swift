//
//  StateModel.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/20/21.
//

import Foundation
//{
//  "data": [
//    {
//      "ID State": "04000US01",
//      "State": "Alabama",
//      "ID Year": 2019,
//      "Year": "2019",
//      "Population": 4903185,
//      "Slug State": "alabama"
//    },
//    {
//      "ID State": "04000US02",
//      "State": "Alaska",
//      "ID Year": 2019,
//      "Year": "2019",
//      "Population": 731545,
//      "Slug State": "alaska"
//    },
//    {
//      "ID State": "04000US04",
//      "State": "Arizona",
//      "ID Year": 2019,
//      "Year": "2019",
//      "Population": 7278717,
//      "Slug State": "arizona"
//    },
//    {
//      "ID State": "04000US05",
//      "State": "Arkansas",
//      "ID Year": 2019,
//      "Year": "2019",
//      "Population": 3017804,
//      "Slug State": "arkansas"
//    },
//    {
//      "ID State": "04000US06",
//      "State": "California",
//      "ID Year": 2019,
//      "Year": "2019",
//      "Population": 39512223,
//      "Slug State": "california"
//    }
//  ],
//  "source": [
//    {
//      "measures": [
//        "Population"
//      ],
//      "annotations": {
//        "source_name": "Census Bureau",
//        "source_description": "The American Community Survey (ACS) is conducted by the US Census and sent to a portion of the population every year.",
//        "dataset_name": "ACS 1-year Estimate",
//        "dataset_link": "http://www.census.gov/programs-surveys/acs/",
//        "table_id": "B01003",
//        "topic": "Diversity",
//        "subtopic": "Demographics"
//      },
//      "name": "acs_yg_total_population_1",
//      "substitutions": []
//    }
//  ]
//}
struct StateModel: Codable {
    
    let id: String
    let state: String
}
