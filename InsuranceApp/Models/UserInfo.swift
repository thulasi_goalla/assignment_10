//
//  UserInfo.swift
//  InsuranceApp
//
//  Created by Asanga Dinesh Perera on 8/20/21.
//

import Foundation

struct UserInfo {
    let userId: String
    let username: String
    let name: String
    let telephone: String
    let email: String
}
