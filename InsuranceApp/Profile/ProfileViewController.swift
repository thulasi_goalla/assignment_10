//
//  ProfileViewController.swift
//  InsuranceApp
//
//  Created by Thulasipathi Goalla on 8/26/21..
//

import UIKit
import PromiseKit
import Alamofire
import SwiftyJSON

class ProfileViewController: UITableViewController{
    
    var hiddenSections = Set<Int>()
    var userArray: [UserModel] = [UserModel]()
    var stateArray: [StateModel] = [StateModel]()
    var userDetailArray: Array = [String]()
    var isRowSelected: Bool = false
    var isRowInserted: Bool = false
    
    let stateIndexPath = 3
    var isState = "true"
    
    let stateCode = ["IL", "FL", "MA"]
    
    let messageCellText = ["Street", "Apt/Suite", "City", "State", "Country", "Zip", "Phone", "Email"]
    var detailCellText = [""]
    
    var tempMessageCellText = ["Street", "Apt/Suite", "City", "State", "Country", "Zip", "Phone", "Email"]
    var tempDetailCellText = [""]
    
    
    
    @IBAction func tappedSave(_ sender: UIButton) {
        
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "PROFILE"

        tableView.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "ReusableCell")
        tableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ReusableCellOne")
        tableView.register(ProfileSaveTableViewCell.self, forCellReuseIdentifier: ProfileSaveTableViewCell.cellId)
        
        let imageView = UIImageView(image: UIImage(named: "Contact"))
        let buttonItem = UIBarButtonItem(customView: imageView)
        self.navigationItem.rightBarButtonItem = buttonItem

        
        if userArray.count > 0 {
            if let userDet = self.userArray as [UserModel]?{
                for row in 0..<userDet.count {
                    
                    self.detailCellText = [userDet[row].location.street.name,
                                           userDet[row].location.street.number,
                                           userDet[row].location.city,
                                           userDet[row].location.state,
                                           userDet[row].location.country,
                                           userDet[row].location.postcode,
                                           userDet[row].phone,
                                           userDet[row].email]
                }
            }
            self.tempDetailCellText = self.detailCellText
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return userArray.count
        } else if section == 1 {
            if self.isRowSelected == true {
                if self.hiddenSections.contains(section) {
                    return userDetailArray.count
                }
                return userDetailArray.count + stateArray.count
            }
            return userDetailArray.count
        }else if section == 2 {
            return 1
        }
        else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 25))
            let label = UILabel()
            label.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width, height: headerView.frame.height)
            if section == 0 {
                label.text = "Profile & Settings"
            } else if section == 1 {
                label.text = "EDIT INFO"
            }
            label.font = .systemFont(ofSize: 16)
            label.textColor = .black
            label.textAlignment = .center
            label.backgroundColor = .white
            headerView.addSubview(label)
            return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableCellOne", for: indexPath) as! ProfileTableViewCell
            if userArray.count > 0 {
                cell.labelOne.text = "\(userArray[indexPath.row].username.title) \(userArray[indexPath.row].username.name) "//"John Peter"
                cell.labelTwo.text = userArray[indexPath.row].id.value//"123456"
                cell.labelThree.text = userArray[indexPath.row].id.name //"john.peter"
                cell.labelFour.text = userArray[indexPath.row].phone//"123-456-7891"
                cell.labelFive.text = userArray[indexPath.row].email//"johnpeter@gmail.com"
                cell.contactImage.downloaded(from: userArray[indexPath.row].picture.large)
                
            }
            return cell
            
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableCell", for: indexPath)
                as! AddressCell
            
            if indexPath.row < tempMessageCellText.count {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideSection(_:)))
                
                cell.addGestureRecognizer(tapGesture)
                
                cell.label.text = tempMessageCellText[indexPath.row]
                
                cell.textfield.text = tempDetailCellText[indexPath.row]
                
                cell.textfield.isHidden = false
                
                cell.messageBubble.backgroundColor =  #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
                
                if tempDetailCellText[indexPath.row] == "true" {
                    
                    cell.textfield.isHidden = true
                    cell.label.textColor = UIColor.black
                    cell.messageBubble.backgroundColor = .white
                }
            }
            return cell
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSaveTableViewCell.cellId, for: indexPath)
                as! ProfileSaveTableViewCell
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            if (self.tableView.cellForRow(at: indexPath) as? AddressCell) != nil {
                if indexPath.row  == stateIndexPath {
                    isRowSelected = true
                }
            }
        }
        
    }
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if (self.tableView.cellForRow(at: indexPath) as? AddressCell) != nil {
                if indexPath.row  == stateIndexPath {
                    isRowSelected = false
                }
            }
        }
    }
    
    
    
    @objc
    private func hideSection(_ sender: UITapGestureRecognizer? = nil) {
        
        let section = 1
        var tappedIndexPath = 0
        var tappedTF = ""
        var changedState = ""
        var hasChangedState = false
        
        if sender?.state == UIGestureRecognizer.State.ended {
            guard let tapLocation = sender?.location(in: self.tableView) else { return  }
            if let tapIndexPath = self.tableView.indexPathForRow(at: tapLocation) {
                if let tappedCell = self.tableView.cellForRow(at: tapIndexPath) as? AddressCell {
                    
                    //do what you want to cell here
                    
                   tappedIndexPath = tapIndexPath.row
                    
                    if tappedCell.textfield.text != nil {
                        tappedTF = tappedCell.textfield.text ?? ""
                        if tappedTF == "true" {
                            changedState = tappedCell.label.text ?? ""
                            hasChangedState = true
                        }
                    }
                }
            }
        }
        func indexPathsForSection() -> [IndexPath] {
            
            var indexPaths = [IndexPath]()
            
            if stateArray.count > 0 {
                if let states = stateArray as [StateModel]?{
                    if !isRowInserted{
                        for row in 1..<self.stateArray.count+1 {
                            indexPaths.append(IndexPath(row: stateIndexPath+row, section: section))
                            tempDetailCellText = detailCellText
                            tempMessageCellText = messageCellText
                            if hasChangedState {
                                tempDetailCellText[stateIndexPath] = changedState
                            }
                        }
                        
                    } else {
                        for row in 1..<self.stateArray.count+1 {
                            indexPaths.append(IndexPath(row: stateIndexPath+row, section: section))
                            
                            tempMessageCellText.insert(states[row-1].state, at: stateIndexPath+row)
                            
                            tempDetailCellText.insert(isState, at: stateIndexPath+row)
                        }
                    }
                }
            }
            return indexPaths
        }
        
        if tappedIndexPath == stateIndexPath || tappedTF == "true" {
            if !isRowInserted {
                self.hiddenSections.remove(section)
                isRowSelected = true
                isRowInserted = true
                self.tableView.insertRows(at: indexPathsForSection(), with: .fade)
                
            } else {
                
                self.hiddenSections.insert(section)
                isRowSelected = false
                isRowInserted = false
                self.tableView.deleteRows(at: indexPathsForSection(), with: .automatic)
            }
        }
        self.tableView.reloadData()
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
